#!/bin/bash 
Encoding="UTF-8"
export SCRFNAME=""; export SCRSNAME=""; export SRCFID=""; export SRCSID=""; export DISMON=""; export XRANDR=""; export HWINFO=""
export OUTCONS=""; export CONS=""; export COND=""; export VONS=""
DIRNAME="`cd  "$(dirname "$0")" ; pwd`/`basename "$0"`"; export DIRNAME="`dirname "${DIRNAME}"`"; source ${DIRNAME}/Screen_Text
StatusSignalCrt() { rm -f /tmp/SGNL 2>/dev/null; sleep 1.5; echo "true" >/tmp/SGNL; }; export -f StatusSignalCrt
ScreenHrwStatus() { export OUSLC=`grep "VGA\|HDMI" /tmp/scr_slc 2>/dev/null`
if [[ $CONS == "true" ]] && [[ $DISMON == 'true' ]] && [[ $OUSLC == "" ]]; then export OUTCONS='false'; export INCONS='true'
elif [[ $CONS == "true" ]] && [[ $DISMON == 'true' ]] && [[ $OUSLC != "" ]]; then export OUTCONS='true'; export INCONS='false'
elif [[ $CONS == "true" ]] && [[ $DISMON != 'true' ]] && [[ $OUSLC == "" ]]; then export OUTCONS='true'; export INCONS='false'
elif [[ $CONS == "true" ]] && [[ $DISMON != 'true' ]] && [[ $OUSLC != "" ]]; then export OUTCONS='false'; export INCONS='true'
elif [[ (($CONS == "false") && ($COND == "true")) ]]; then export OUTCONS='false'; export INCONS='false'; fi; }; export -f ScreenHrwStatus 
AvailableScreenResulation() { Sresulation=`$XRANDR |grep -A5 "$(cat /tmp/scr_slc)"|awk "/''/{f=0} f; /$(cat /tmp/scr_slc)/{f=1}"|grep -v i|awk '{print $1,$2}'|grep -E '[0-9]x[0-9]'`; SetRes=""; export ResScaleA=`echo "$Sresulation"|sed -n 1p|cut -d' ' -f1`; export ResScaleB=`echo "$Sresulation"|sed -n 2p|cut -d' ' -f1`; export ResScaleC=`echo "$Sresulation"|sed -n 3p|cut -d' ' -f1`; export ResScaleD=`echo "$Sresulation"|sed -n 4p|cut -d' ' -f1`
SlcResul=`echo "$Sresulation"|awk '/*/{print $1}'`; if [[ $SlcResul == $ResScaleA ]]; then export Rsec=4; elif [[ $SlcResul == $ResScaleB ]]; then export Rsec=3; elif [[ $SlcResul == $ResScaleC ]]; then export Rsec=2; elif [[ $SlcResul == $ResScaleD ]]; then export Rsec=1; else export Rsec=1; fi; }; export -f AvailableScreenResulation
SetScreenResulation() { AvailableScreenResulation; if [[ $SReS -eq 4 ]]; then SetRes=$ResScaleA; elif [[ $SReS -eq 3 ]]; then SetRes=$ResScaleB
elif [[ $SReS -eq 2 ]]; then SetRes=$ResScaleC; elif [[ $SReS -eq 1 ]]; then SetRes=$ResScaleD; fi
if [[ ! -z $SetRes ]]; then $XRANDR --output $(cat /tmp/scr_slc) --mode $SetRes; fi; }; export -f SetScreenResulation
function ScreenResulationConfigure() { AvailableScreenResulation; echo '<vseparator> </vseparator> <vseparator> </vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$SCRESI\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text>
<hscale width-request="'$1'" height-request="'$2'" scale-min="1" scale-max="4" scale-step="1" value-pos="1" > <variable>SReS</variable> <input>echo "'$Rsec'"</input> <action>SetScreenResulation</action> <item>"1 |2|<span fgcolor='"'#e4a725'"' bgcolor='"'#21211f'"'> '$ResScaleD' </span>"</item> <item>"2 |2|<span fgcolor='"'#e4a725'"' bgcolor='"'#21211f'"'> '$ResScaleC' </span>"</item> <item>"3 |2|<span fgcolor='"'#e4a725'"' bgcolor='"'#21211f'"'> '$ResScaleB' </span>"</item> <item>"4 |2|<span fgcolor='"'#e4a725'"' bgcolor='"'#21211f'"'> '$ResScaleA' </span>"</item> </hscale>'; }; export -f ScreenResulationConfigure
function BacklightConfigure() { export XbackL=`xbacklight -get|cut -d. -f1`; XbackL=$(( $XbackL+1)) ; export ScrNo=`ps -u $(id -u) -o pid= | while read pid; do cat /proc/$pid/environ 2>/dev/null | tr '\0' '\n' | grep '^DISPLAY=:'; done | grep -o ':[0-9]*' | sort -u`
echo '<vseparator> </vseparator> <vseparator> </vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$SLIGHT\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text>
<hscale width-request="'$1'" height-request="'$2'" scale-min="9" scale-max="99" scale-step="1" value-pos="1" > <variable>Bcklght</variable> <input>echo "'$XbackL'"</input> <action>xbacklight -d "'"$ScrNo"'" -set "$Bcklght"</action> </hscale> ' ; } ; export -f BacklightConfigure
function BrightnessConfigure() { if [[ `echo "$XRANDV"|grep -A7 "$(cat /tmp/scr_slc)"|grep Brightness|cut -d: -f2|xargs|cut -d. -f1` -eq 1 ]]
then export IBRG='99'; else export IBRG=`echo "$XRANDV"|grep -A7 "$(cat /tmp/scr_slc)"|grep Brightness|cut -d: -f2|xargs|cut -d. -f2`; fi
echo '<vseparator> </vseparator> <vseparator> </vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$SBRRG\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text>
<hscale width-request="'$2'" height-request="'$3'" scale-min="44" scale-max="99" scale-step="1" value-pos="1" > <variable>Bright</variable> <input>echo '$IBRG'</input> <action>'$XRANDR' --output $(cat /tmp/scr_slc) --brightness "0.$Bright"</action> </hscale> ' ; } ; export -f BrightnessConfigure
ScreenReturn() { if test -e /tmp/xnull; then $XRANDR --output `cat /tmp/scr_slc` --rotate normal; rm /tmp/xnull; else $XRANDR --output `cat /tmp/scr_slc` --rotate inverted; echo >/tmp/xnull; fi; }; export -f ScreenReturn
All_Process_RefresH() { kill -9 `ps -ax | grep gtkdialog|grep -v grep|awk '{print $1}'`; nohup &>/dev/null $DIRNAME/Screen_Refresh.sh &
}; export -f All_Process_RefresH
function ProcessedXAMonitor() { if [[ "$SLCA" == "true" ]] && [[ `cat /tmp/SGNL` == "true" ]]; then
echo "$SRCFID" > /tmp/scr_slc; All_Process_RefresH; fi; }; export -f ProcessedXAMonitor
function ProcessedXBMonitor() { if [[ "$SLCD" == "true" ]] && [[ `cat /tmp/SGNL` == "true" ]] && [[ $COND == 'true' ]]; then 
echo "$SRCSID" > /tmp/scr_slc; All_Process_RefresH; else All_Process_RefresH; fi; }; export -f ProcessedXBMonitor
function MonitorSlectionMenu() { if [[ $OUSLC != "" ]] && [[ $COND == 'true' ]]; then 
echo '<vseparator> </vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$SCXISC\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text>
 <hbox homogeneous="true"> <radiobutton active="false" inconsistent="'$INCONS'"> <label>PC</label> <variable>SLCA</variable> <action>ProcessedXAMonitor</action> </radiobutton> <button width-request="130" height-request="35"> <label>"'$SCRRVR'"</label> <action>ScreenReturn</action> </button> <radiobutton active="true" inconsistent="'$OUTCONS'"> <label>Harici</label> <variable>SLCB</variable> <action>ProcessedXAMonitor</action> </radiobutton> </hbox> <vseparator> </vseparator> <vseparator> </vseparator>'
else echo '<vseparator> </vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$SCXISC\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text>
<hbox homogeneous="true"> <radiobutton active="true" inconsistent="'$INCONS'"> <label>PC</label> <variable>SLCC</variable> <action>ProcessedXBMonitor</action> </radiobutton> <button width-request="130" height-request="35"> <label>"'$SCRRVR'"</label>
 <action>ScreenReturn</action> </button> <radiobutton active="false" inconsistent="'$OUTCONS'"> <label>Harici</label> <variable>SLCD</variable> <action>ProcessedXBMonitor</action> </radiobutton> </hbox>
 <vseparator> </vseparator> '; fi; } 
function screenFinformation() { export information='<window title=" " resizable="false" width-request="550" height-request="500"> <vbox homogeneous="true"> <frame '$SCRFNAME' Bilgileri> <vseparator> </vseparator> <vseparator> </vseparator> <vseparator> </vseparator> <vseparator> </vseparator> <text> <input>hwinfo --monitor |grep -B1 -A30 "$SCRFNAME"|sed -n '/Hardware/,/Attached/p'</input> </text> </frame> </vbox> </window>'; gtkdialog --center --program=information; }; export -f screenFinformation
function screenSinformation() { export information='<window title=" " resizable="false" width-request="550" height-request="500"> <vbox homogeneous="true"> <frame '$SCRSNAME' Bilgileri> <vseparator> </vseparator> <vseparator> </vseparator> <vseparator> </vseparator> <vseparator> </vseparator> <text> <input>hwinfo --monitor |grep -B1 -A30 "$SCRSNAME"|sed -n '/Hardware/,/Attached/p'</input> </text> </frame> </vbox> </window>'; gtkdialog --center --program=information; }; export -f screenSinformation
 function ConnectMonitorInformation() { if [[ -z $SCRSNAME ]]; then echo '<vseparator></vseparator> <vseparator></vseparator> <vbox space-expand="true"> <button width-request="190" height-request="190"> <label>'$SCRFNAME'</label> <action>screenFinformation</action>
</button> </vbox> <vseparator> </vseparator> <vseparator> </vseparator>'
else echo '<vseparator></vseparator> <vseparator></vseparator> <vbox space-expand="true"> <button width-request="190" height-request="95">
<label>'$SCRFNAME'</label> <action>screenFinformation</action> </button> <vseparator> </vseparator> <vseparator> </vseparator> <button width-request="190" height-request="95"> <label>'$SCRSNAME'</label> <action>screenSinformation</action> </button> </vbox> <vseparator> </vseparator> <vseparator> </vseparator>' ; fi; } 
function SelectedPrimaryMonitor() { if [[ $CONS == 'true' ]] && [[ `$XRANDR |grep -E 'connected primary [1-9]+' | sed -e 's/\([A-Z0-9]\+\) connected.*/\1/'` != $1 ]]; then $XRANDR --output $1 --primary; fi; }; export -f SelectedPrimaryMonitor
function SelectedCurrentMonitor() { if [[ `$XRANDR |grep -E 'connected primary [1-9]+' | sed -e 's/\([A-Z0-9]\+\) connected.*/\1/'` != $1 ]] || [[ `echo "$XRACTV"|wc -l` -ne 1 ]]; then $XRANDR --output $1 --auto --primary; counter=0; while [  $counter -lt 50 ]; do
if [[ `$XRANDR |grep -E 'connected primary [1-9]+' |sed -e 's/\([A-Z0-9]\+\) connected.*/\1/'` == $1 ]]; then sleep 1; $XRANDR --output $2 --off; break; fi; sleep .2; let counter=counter+1; done; sleep 1; if [[ "$DISMON" == "true" ]] || [[ "$1" == "$SRCFID" ]]; then echo "$1" > /tmp/scr_slc; All_Process_RefresH ; fi; fi; }; export -f SelectedCurrentMonitor
function SelectedStretchMonitor() { $XRANDR --output $1 --primary --auto --output $2 --right-of $1 --auto
if [[ "$DISMON" == "true" ]] || [[ $CONS == 'false' ]]; then All_Process_RefresH; fi; }; export -f SelectedStretchMonitor
function SelectedProjectionMonitor() { $XRANDR --output $1 --auto --output $2 --auto --scale 1x1 --same-as $1
if [[ "$DISMON" == "true" ]] || [[ $CONS == 'false' ]]; then All_Process_RefresH; fi; }; export -f SelectedProjectionMonitor
function MultiScreenOptions() { echo '<vseparator></vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$SCRSEL\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text>
<vbox homogeneous="true"> <button> <label>" '$PRSCRO' "</label> <sensitive>"'"$CONS"'"</sensitive> <action>SelectedPrimaryMonitor $SRCFID</action> </button> <button> <label>" '$PRSCRT'  "</label> <sensitive>"'"$CONS"'"</sensitive> <action>SelectedPrimaryMonitor $SRCSID</action> </button> </vbox> <vseparator></vseparator> <vseparator></vseparator> <vseparator></vseparator> <vbox homogeneous="true"> 
<button> <label>" '$CRSCRO'  "</label> <sensitive>"'"$COND"'"</sensitive> <action>SelectedCurrentMonitor $SRCFID $SRCSID</action> </button>
<button> <label>" '$CRSCRT' "</label> <sensitive>"'"$COND"'"</sensitive> <action>SelectedCurrentMonitor $SRCSID $SRCFID</action> </button> 
<button> <label>" '$STRSCR' "</label> <sensitive>"'"$COND"'"</sensitive> <action>SelectedStretchMonitor $SRCFID $SRCSID</action> </button>
<button> <label>" '$PRJSCR' "</label> <sensitive>"'"$COND"'"</sensitive> <action>SelectedProjectionMonitor $SRCFID $SRCSID</action> </button> </vbox> <vseparator></vseparator> <vseparator></vseparator> <vseparator> </vseparator> <vseparator> </vseparator>'; } 
ChangeVolumeSet() { if [[ `amixer get Master | awk -F"%] " 'NR == 6 {print $2}'` == '[on]' ]]; then amixer set Master $VLevel%
else amixer set Master unmute; amixer set Master $VLevel%; fi; }; export -f ChangeVolumeSet
function VolumeControlConfigure() { MasterV=`amixer get Master | egrep 'Playback.*?\[o'|cut -d% -f1|cut -d[ -f2|tail -n1`
echo '<vseparator></vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$VLMSET\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text>
<vbox space-expand="true"> <button width-request="120" height-request="33"> <label>" '$VOMUTE' "</label> <action>amixer set Master mute</action> </button> <vseparator> </vseparator> <vseparator> </vseparator> <button width-request="120" height-request="33"> <label>" '$VLMPC' "</label> <sensitive>"'"$VONS"'"</sensitive> <action type="exit">exit 0</action> </button> <vseparator> </vseparator> <vseparator> </vseparator> <button width-request="120" height-request="33"> <label>" '$VLHDM' "</label> <sensitive>"'"$VONS"'"</sensitive> <action type="exit">exit 0</action> </button> </vbox> <vseparator> </vseparator> <vseparator> </vseparator> <vscale width-request="'$1'" height-request="'$2'" scale-min="0" scale-max="99" scale-step="1" draw-value="true" show-fill-level="true" fill-level="100" restrict-to-fill-level="false" update-policy="1" value-pos="0" inverted="true"> <variable>VLevel</variable> <input>echo '$MasterV'</input> <action signal="value_changed">ChangeVolumeSet</action> </vscale> <vseparator></vseparator> <vseparator> </vseparator> <vseparator> </vseparator>'; } 
AvailableScrGamma() {
export XGMM='
0.8  1.2   8
0.9   1.1   7
1.0   1      6
1.1   0.9   5
1.2   0.85  4
1.3   0.8    3
1.4   0.7    2
1.7   0.6    1'
GAMA=`$XRANDR --verbose|grep -A7 "$(cat /tmp/scr_slc)"|sed -n '/Gamma/p'|cut -d: -f2-|xargs`
RGMA=`echo "$XGMM" |grep "^$(echo "$GAMA"|cut -d: -f1|cut -c1-3)"`
 export RGMM=`echo "$RGMA"|awk '{print $3}'`
GGMA=`echo "$XGMM" |grep "^$(echo "$GAMA"|cut -d: -f2|cut -c1-3)"`
 export GGMM=`echo "$GGMA"|awk '{print $3}'`
BGMA=`echo "$XGMM" |grep "^$(echo "$GAMA"|cut -d: -f3|cut -c1-3)"`
 export BGMM=`echo "$BGMA"|awk '{print $3}'`; }; export -f AvailableScrGamma
function set_rgamma() { AvailableScrGamma; RGMB=`echo "$XGMM" |grep "${Rgamma}$"|awk '{print $2}'`; GGMB=`echo "$GGMA"|awk '{print $2}'`; BGMB=`echo "$BGMA"|awk '{print $2}'`; $XRANDR --output `cat /tmp/scr_slc` --gamma $RGMB:$GGMB:$BGMB; }; export -f set_rgamma
function set_ggamma() { AvailableScrGamma; GGMB=`echo "$XGMM" |grep "${Ggamma}$"|awk '{print $2}'`; RGMB=`echo "$RGMA"|awk '{print $2}'`; BGMB=`echo "$BGMA"|awk '{print $2}'`; $XRANDR --output `cat /tmp/scr_slc` --gamma $RGMB:$GGMB:$BGMB; }; export -f set_ggamma
function set_bgamma() { AvailableScrGamma; BGMB=`echo "$XGMM" |grep "${Bgamma}$"|awk '{print $2}'`
GGMB=`echo "$GGMA"|awk '{print $2}'`; RGMB=`echo "$RGMA"|awk '{print $2}'`; $XRANDR --output `cat /tmp/scr_slc` --gamma $RGMB:$GGMB:$BGMB; }; export -f set_bgamma
function GammaConfigure() { AvailableScrGamma; echo '<vseparator> </vseparator> <vseparator> </vseparator> <text sensitive="true" use-markup="true" wrap="false" angle="0"> <input>echo "\<b>\<span color=\"#cecece\" size=\"medium\">$SCGAM\</span>\</b>" | sed "s%\\\%%g" | sed "s%\\\\n\\\\n\\\\n%%g"</input></text> <hscale width-request="340" height-request="38" scale-min="1" scale-max="8" scale-step="1" value-pos="1"> <variable>Rgamma</variable> <input>echo '$RGMM'</input> <action>set_rgamma</action> 
<item>"1 |2|<span fgcolor='"'white'"' bgcolor='"'#801e1e'"'> 1 </span>"</item> <item>"6|2|<span fgcolor='"'white'"' bgcolor='"'#750d0d'"'> 6 </span>"</item> <item>"8|2|<span fgcolor='"'white'"' bgcolor='"'#430000'"'> 8 </span>"</item> </hscale> <vseparator> </vseparator>
 <hscale width-request="340" height-request="38" scale-min="1" scale-max="8" value-pos="1"> <variable>Ggamma</variable> <input>echo '$GGMM'</input> <action>set_ggamma</action> <item>"1 |2|<span fgcolor='"'white'"' bgcolor='"'#2e7920'"'> 1 </span>"</item> <item>"6|2|<span fgcolor='"'white'"' bgcolor='"'#0d5b0a'"'> 6 </span>"</item> <item>"8|2|<span fgcolor='"'white'"' bgcolor='"'#033800'"'> 8 </span>"</item> </hscale> 
<vseparator> </vseparator> <hscale width-request="340" height-request="38" scale-min="1" scale-max="8" value-pos="1"> <variable>Bgamma</variable> <input>echo '$BGMM'</input> <action>set_bgamma</action> <item>"1 |2|<span fgcolor='"'white'"' bgcolor='"'#2a2f8c'"'> 1 </span>"</item> <item>"6|2|<span fgcolor='"'white'"' bgcolor='"'#10146a'"'> 6 </span>"</item> 
<item>"8|2|<span fgcolor='"'white'"' bgcolor='"'#000340'"'> 8 </span>"</item> </hscale>'; }
function ScreenMainDialog() { export Screen_Main_Menu='<window title="MultiHead" icon-name="multihead-icon" decorated="true" resizable="false" border-width="7" border-height="0"> <vbox> <hbox> <vbox> <frame> <vbox> '"$(MultiScreenOptions)"' </vbox> <vbox> '"$(ConnectMonitorInformation)"' </vbox> </frame> </vbox> <vbox> <frame> <vbox> '"$(MonitorSlectionMenu)"' </vbox> <vbox>'"$(ScreenResulationConfigure 340 50)"' </vbox> <vbox>'"$(BacklightConfigure 340 30)"' </vbox> <vbox>'"$(BrightnessConfigure 340 30)"'</vbox> <vbox>'"$(GammaConfigure)"' </vbox> </frame> 
 <frame> <hbox space-expand="true"> <button width-request="140" height-request="35"> <label>"  '$PRCLS'"</label> <input file stock="gtk-close"></input> <action>${DIRNAME}/Screen_Stop.sh &</action> <action type="exit">exit 0</action> </button> <button width-request="140" height-request="35"> <label>"  '$PRREF'"</label> <input file stock="gtk-refresh"></input> <action>${DIRNAME}/Screen_Refresh.sh &</action> </button> </hbox> </frame> </vbox> <frame> <vbox>'"$(VolumeControlConfigure 50 350 2)"'</vbox> </frame> </hbox> </vbox> <action signal="hide">exit:Exit</action> </window>'; }
StartMainFunctions() { [[ -e /tmp/scr_slc ]] || echo "$SRCXID" >/tmp/scr_slc; StatusSignalCrt &
  ScreenHrwStatus; ScreenMainDialog
  nohup &>/dev/null gtkdialog --center --program=Screen_Main_Menu &
  sleep 3; while [ true ]; do sleep 1; if [[ $1 -ne `$XRANDR |grep -Ew 'connected'|wc -l` ]]; then
  break; fi; done; All_Process_RefresH; }
SystemScreenInfo() { export SRCXID=`echo "${XRANDV}" |grep -E 'connected primary [1-9]+' | sed -e 's/\([A-Z0-9]\+\) connected.*/\1/'`
if [[ `echo "${HWINFO}" | wc -l` -eq 1 ]]; then
if [[ `echo "${XRANDV}" |grep -Ew 'connected'|wc -l` -eq 1 ]]; then export SCRFNAME="`echo "$HWINFO"|xargs`"
export SRCFID=`echo "${XRANDV}" |grep -E 'connected primary [1-9]+' | sed -e 's/\([A-Z0-9]\+\) connected.*/\1/'`; export CONS='false'; 
export COND='false'; export VONS='false'; StartMainFunctions 1; fi
elif [[ `echo "${HWINFO}"| wc -l` -eq 2 ]]; then if [[ `echo "${XRANDV}" |grep -Ew 'connected'|wc -l` -eq 2 ]]; then 
export SCRFNAME=`echo "${HWINFO}"|head -n1|xargs`; export SCRSNAME=`echo "${HWINFO}"|tail -n1|xargs`
 export SRCFID=`echo "${XRANDV}" |grep -E 'connected' |  sed -e 's/\([A-Z0-9]\+\) connected.*/\1/'|grep -v 'VGA\|HDMI'`
 export SRCSID=`echo "${XRANDV}" |grep -E 'connected' |  sed -e 's/\([A-Z0-9]\+\) connected.*/\1/'|grep -v "$SRCFID"`; export CONS='true'
 if [[ `$XRANDR --listmonitors|grep +|wc -l` -lt 2 ]]; then echo "$SRCXID" >/tmp/scr_slc; export DISMON='true'; fi
 if [[ ! -z $DISMON ]] || [[ `echo "$XRACTV"|wc -l` -ne 2 ]]; then export CONS='false'; export VONS='true'; fi
 if [[ `echo "$XRACTV"|grep HDMI` == "" ]]; then export VONS='false'; fi
 export COND='true'; StartMainFunctions 2; fi; fi; }
 export GTKDIALOG=gtkdialog; which xrandr &>/dev/null && export XRANDR=`which xrandr`
 which hwinfo &>/dev/null && HWINFO=`which hwinfo`; export HWINFO=`$HWINFO --monitor --short|grep -v ':'|grep -Ei '[a-z:0-9]+'`
 export XRANDV=`$XRANDR --verbose|grep -v "$($XRANDR --verbose|grep "disconnected" -A9)"`
 export XRACTV=`$XRANDR --listactivemonitors|grep ': +'`
if [[ ! -z $XRANDR ]] && [[ ! -z $HWINFO ]]; then SystemScreenInfo; fi
exit 0
