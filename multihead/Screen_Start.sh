#!/bin/bash
DIRNAME="`cd  "$(dirname "$0")" ; pwd`/`basename "$0"`"
DIRNAME="`dirname "${DIRNAME}"`"
PID_SCRIPT=`pidof -x $(basename $0)`
ICONS=~/.local/share/icons/hicolor/48x48/apps/
COUNT=0

[[ -d $ICONS ]] || mkdir -p $ICONS
[[ -e $ICONS/multihead-icon.png ]] || cp -f $DIRNAME/multihead-icon.png $ICONS
while [[ $COUNT -lt 3 ]]; do
check_stop() { while true; do sleep 1
if [[ `ps axu | grep -v grep|grep Screen_Main_Menu|wc -l` -gt 1 ]]; then
kill `ps ax | grep gtkdialog|grep -v grep|awk '{print $1}'|xargs` 2>/dev/null
kill -9 `lsof | grep ${DIRNAME} | awk '{print $2}' | grep -v ${PID_SCRIPT} | xargs` 2>/dev/null
kill -9 `ps axu | grep ${DIRNAME} | grep -v ${PID_SCRIPT} | grep /bin/bash | grep -v grep | awk '{print $2}' | xargs` 2>/dev/null
kill -9 `ps ax | grep gtkdialog | grep Screen_Main_Menu | awk '{print $1}'` 2>/dev/null
break; fi; done; }
check_start() { rm /tmp/scr_slc 2>/dev/null
if [[ `ps axu | grep -v grep|grep Screen_Main_Menu|wc -l` -ge 1 ]]; then check_stop
else nohup &>/dev/null bash -c "GTK2_RC_FILES=$DIRNAME/rc/Screenrc $DIRNAME/Screen_Main.sh &"
sleep 2; check_stop; fi; }
if [[ `ps axu | grep -v 'grep'|grep gtkdialog|grep Screen_Main_Menu` == "" ]]; then check_start
else nohup &>/dev/null ${DIRNAME}/Screen_Refresh.sh &
break; fi; let COUNT=COUNT+1 ; done; exit 0
