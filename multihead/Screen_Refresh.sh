#! /bin/bash

DIRNAME="`cd  "$(dirname "$0")" ; pwd`/`basename "$0"`"
DIRNAME="`dirname "${DIRNAME}"`"
PID_SCRIPT=`pidof -x $(basename $0)`
TEST=0
while [[ $TEST -lt 3 ]]; do
kill `ps -ax | grep gtkdialog|grep -v grep|awk '{print $1}'|xargs` 2>/dev/null
kill -9 `lsof | grep ${DIRNAME} | awk '{print $2}' | grep -v ${PID_SCRIPT} | xargs` 2>/dev/null
kill -9 `ps aux | grep ${DIRNAME} | grep -v ${PID_SCRIPT} | grep /bin/bash | grep -v grep | awk '{print $2}' | xargs` 2>/dev/null
kill -9 `ps ax | grep gtkdialog | grep Screen_Main_Menu | awk '{print $1}'` 2>/dev/null
ps -ax | grep gtkdialog|grep -v grep || nohup &>/dev/null bash -c "GTK2_RC_FILES=$DIRNAME/rc/Screenrc $DIRNAME/Screen_Main.sh &"
sleep 2
ps -ax | grep gtkdialog|grep -v grep  && break
let TEST=TEST+1; done
exit 0
